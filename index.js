const express = require("express");
const bodyParser = require("body-parser");
const fs = require("fs");
const PORT = process.env.PORT || 3001;
const app = express();

app.use(bodyParser.text({ type: "text/plain" }));
app.use(bodyParser.urlencoded({ extended: true }));

app.get("/", function(req, res) {
  res.send("HELLO WORLD");
});

app.get("/readFile", function(req, res) {
  fs.readFile(__dirname + "/" + "test.txt", "utf8", function(err, data) {
    res.end(JSON.stringify(data));
  });
});

app.post("/writeFile", function(req, res) {
  console.log(req.body);
  fs.writeFile(__dirname + "/writetest.txt", JSON.stringify(req.body), function(
    err
  ) {
    if (err) {
      return console.log(err);
    }
    res.send("File saved");
  });
});

app.get("/product/:num1/:num2", (req, res) => {
  let result = req.params.num1 * req.params.num2;
  console.log(result);
  res.send(JSON.stringify(result));
});

app.listen(PORT, (err, server) => {
  if (!err) console.log(`Server started at ${PORT}`);
  else console.log(err);
});
